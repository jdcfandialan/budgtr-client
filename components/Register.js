import {useState, useEffect} from 'react';
import Router from 'next/router';
import {Button, Form, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName]  = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [show, setShow] = useState(false);
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    useEffect(() => {
        if((password === confirmPassword) && (firstName !== '') && (lastName !== '') && (email !== '') && (password !== '') && (confirmPassword !== '')){
            setIsActive(true)
        }

        else setIsActive(false)
    }, [firstName, lastName, email, password, confirmPassword])

    function registerUser(e){
		e.preventDefault();
		
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            //if registration is successful, redirect to login
            if(data === true){
                Swal.fire('Success!', 'User registration successful!', 'success')
                handleClose();
				Router.push('/')
            }

            else{
                Swal.fire('Something went wrong...', 'User registration failed.', 'error')
            }
        })

	}
    
    return (
        <React.Fragment>
            <Button block variant="light" onClick={handleShow}>Create an Account</Button>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Create a new user</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={e => registerUser(e)}>
                        <Form.Group>
                            <Form.Control placeholder="First Name" type="text" name="firstName" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control placeholder="Last Name" type="text" name="lastName" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control placeholder="Email" type="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control placeholder="Password" type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Control placeholder="Confirm Password" type="password" name="confirmPassword" value={confirmPassword} onChange={(e)=>setConfirmPassword(e.target.value)} />
                        </Form.Group>

                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Create User</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                        
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>  
    )
}