import {ListGroup, Button} from 'react-bootstrap';
import DeleteAccountButton from './DeleteAccountButton';

export default function AccountDetails({account, setShowDetails, setActiveAccount, refreshAccounts, activeAccount}){
    function showDetails(){
        setShowDetails(true);
        setActiveAccount(account);
    }

    const listStyle = (accountId) => {
        if(accountId === activeAccount._id){
            return ({
                borderLeft: '2px solid blue'
            })
        }
    }
    
    return (
        <React.Fragment>
            <ListGroup.Item as="li" className="mt-3 mb-1" action onClick={showDetails} className="d-flex align-items-center justify-content-between" style={listStyle(account._id)}>
                {account.bankName}
                <DeleteAccountButton setShowDetails={setShowDetails} setActiveAccount={setActiveAccount} refreshAccounts={refreshAccounts} account={account} />
            </ListGroup.Item>
        </React.Fragment>
    )
}