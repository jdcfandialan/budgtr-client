import {useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import UserContext from '../UserContext';
// import AddAccountType from './AddAccountType';
// import AddAccountCurrency from './AddAccountCurrency';
// import AddPostCategory from './AddPostCategory';
// import AddAcountCurrency from './AddAccountCurrency';

export default function NavBar(){
    const {user} = useContext(UserContext);

    return (
        <React.Fragment>
            <Navbar expand="lg">
                {
                    (user.id !== null) ?
                    <Navbar.Brand href="/home">budgtr.</Navbar.Brand> : <Navbar.Brand href="#home">budgtr.</Navbar.Brand>
                }
                <Navbar.Toggle aria-controls="basic-nav-bar" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    </Nav>

                    {
                        //render if there is a user logged in
                        (user.id !== null) ?
                        <Nav>
                            <Nav.Link href="/Logout">Sign Out</Nav.Link>
                        </Nav> : null
                    }
                </Navbar.Collapse>
            </Navbar>
        </React.Fragment>
    )
}