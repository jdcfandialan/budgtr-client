import {useState, useContext, useEffect} from 'react';
import {Form, Button, Modal} from 'react-bootstrap';
import Router from 'next/router';
import {GoogleLogin} from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Register from './Register';

export default function Login(){
    const {setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((email !== '') && (password !== '')){
            setIsActive(true);
        }

        else setIsActive(false);
	}, [email, password])

    function authenticate(e){
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
		.then(data => {
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken);
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id
					})
					Router.push('/home');
				})
			}

			else{
				Swal.fire('Login failed!', 'Something went wrong, please try again', 'error')
			}
		})
	}

    const captureLoginResponse = (response) => {
		const payload = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		}

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verifyGoogleIdToken`, payload)
		.then(res => res.json())
		.then(data => {
			if(data.accessToken){
				localStorage.setItem('token', data.accessToken);
				
				setUser({
					id: data._id
				});
				
				Router.push('/home');
			}

			else{
				Swal.fire('Login failed!', 'Something went wrong, please try again', 'error')
			}
		})
	}

    return (
        <div className="px-5 mt-5" style={{borderLeft: '1px solid #A9A9A9'}}>
            <Form className="mt-5" onSubmit={(e) => authenticate(e)}>
                <Form.Group>
                    <Form.Control placeholder="Email" type="email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </Form.Group>

                <Form.Group>
	                <Form.Control placeholder="Password" type="password" name="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
	            </Form.Group>

                {
                    (isActive) ?
					<Button variant="outline-primary" type="submit">Login</Button> : <Button variant="outline-primary" type="submit" disabled>Login</Button>
                }
            </Form>
			
			<hr />

			<div className="d-flex flex-column align-items-center justify-content-center">
				<GoogleLogin clientId='192671143805-5269mmsqghgmvr4ni6u217djtuja5c0m.apps.googleusercontent.com' onSuccess={captureLoginResponse} onFailure={captureLoginResponse} cookiePolicy={'single_host_origin'} render ={renderProps => (
						<Button block variant="light" onClick={renderProps.onClick} disabled={renderProps.disabled} >Continue with Google</Button>
					)} />

				<p>or</p>

				<Register />
			</div>
        </div>
    )
}