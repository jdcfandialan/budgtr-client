import {useState, useEffect} from 'react';
import {Modal, Button} from 'react-bootstrap';
import TrendChart from './TrendChart';

export default function UserTrends(){
    const [show, setShow] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    return (
        <React.Fragment>
            <Button variant="light" block onClick={handleShow} className="mb-2">Show User Trends</Button>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>User Trends</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <TrendChart />
                </Modal.Body>
            </Modal>
        </React.Fragment>
        
    )
}