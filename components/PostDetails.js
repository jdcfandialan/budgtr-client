import {useState, useEffect} from 'react';
import {Button, ListGroup, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function PostDetails({accountPost, currencyId, account, refreshPost, balance, refreshAccounts, activeAccount, setActiveAccount, oldAmount, oldTypeId}){
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);
    const [postTypes, setPostTypes] = useState([])
    const [postCategories, setPostCategories] = useState([]);
    const [postName, setPostName] = useState(accountPost.postName);
    const [amount, setAmount] = useState(accountPost.amount);
    const [postTypeId, setPostTypeId] = useState(accountPost.postTypeId);
    const [postCategoryId, setPostCategoryId] = useState(accountPost.postCategoryId);
    const [isTypeChanged, setIsTypeChanged] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    //fetch all post Types
    useEffect(()=>{
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/postTypes`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setPostTypes(data)
            }

            else setPostTypes([])
        })
    }, [])

    //fetch all post categories
    useEffect(()=>{
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/postCategories`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setPostCategories(data)
            }

            else setPostCategories([])
        })
    }, [show])

    //form validation
    useEffect(() => {
        if(amount > 0){
            setIsActive(true);
        }

        else setIsActive(false);
    }, [amount])

    useEffect(() => {
        if(oldTypeId !== postTypeId){
            setIsTypeChanged(true)
        }

        else{
            setIsTypeChanged(false)
        }
    }, [amount, postTypeId])

    function editPost(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/${accountPost._id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                postName: postName,
                amount: amount,
                postTypeId: postTypeId,
                postCategoryId: postCategoryId,
                accountId: account._id
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                let newBalance = 0;

                if(isTypeChanged){
                    let tempAmount = amount;
                    
                    if(postTypeId === 'Expenditure'){
                        tempAmount *= -1;
                    }

                    newBalance = parseFloat(balance) + (parseFloat(tempAmount)*2);
                }

                else newBalance = balance

                fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/${account._id}`,{
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        bankName: account.bankName,
                        amount: newBalance,
                        accountTypeId: account.accountTypeId,
                        accountCurrencyId: account.accountCurrencyId
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        handleClose();
                        refreshAccounts();
                        refreshPost();
                        setActiveAccount({
                            ...activeAccount,
                            amount: newBalance
                        })
                        Swal.fire('Success!', 'Post successfully created!', 'success')
                    }
                })
            }   

            else{
                Swal.fire('Error!', 'Failed to create post!', 'error')
                handleClose()
            }
        })
    }

    function deletePost(){
        Swal.fire({
            title: `Delete Post?`,
            text: 'This will permanently remove your post from the database. Continue?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Delete'
        })
        .then(result => {
            if(result.value){
                let tempAmount = amount;
                if(postTypeId === 'Income'){
                    tempAmount *= -1
                }

                const newBalance = parseFloat(balance) + parseFloat(tempAmount);

                fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/${account._id}`,{
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        bankName: account.bankName,
                        amount: newBalance,
                        accountTypeId: account.accountTypeId,
                        accountCurrencyId: account.accountCurrencyId
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/${account._id}/${accountPost._id}`, {
                            method: 'DELETE'
                        })
                        .then(res => res.json())
                        .then(data => {
                            if(data){
                                refreshAccounts();
                                refreshPost();
                                setActiveAccount({
                                    ...activeAccount,
                                    amount: newBalance
                                })
                                Swal.fire('Deleted!', 'Account has been successfully deleted', 'success')
                            }
                        })
                    }
                })
            }
        })
    }

    const postStyle = (postTypeId) => {
        if(postTypeId === 'Income'){
            return ({
                borderLeft: '2px solid green'
            })
        }

        else return ({
            borderLeft: '2px solid red'
        })
    }

    return (
        <React.Fragment>
            <ListGroup.Item as="li" key={accountPost._id} style={postStyle(accountPost.postTypeId)}>
                <div className="d-flex justify-content-between">
                    <div className="d-flex flex-column justify-content-center">
                        <h6>
                            {accountPost.postCategoryId}
                            {
                                (accountPost.postName !== '') ?
                                <span> ({accountPost.postName})</span> : null
                            }
                        </h6>
                        {
                            (accountPost.postTypeId === 'Income') ?
                            <p className="text-success">+ {currencyId} {accountPost.amount}<span className="text-secondary"> ({accountPost.runningBalance})</span></p>
                            :
                            <p className="text-danger">- {currencyId} {accountPost.amount}<span className="text-secondary"> ({accountPost.runningBalance})</span></p>
                        
                        }
                    </div>
                    
                    <div className="d-flex flex-column align-items-center justify-content-center">
                        <Button size="sm" variant="light" onClick={handleShow}><img src="pencil.png" height="16rem" width="16rem" /></Button>
                        <Button size="sm" variant="light" onClick={deletePost}><img src="criss-cross.png" height="16rem" width="16rem" /></Button>
                    </div>
                </div>
            </ListGroup.Item>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Post</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => editPost(e)}>
                        <Form.Group>
                            <Form.Label>Type (Income or Expenditure)</Form.Label>
                            <Form.Control as="select" value={postTypeId} onChange={e => setPostTypeId(e.target.value)}>
                                {
                                    postTypes.map(postType => {
                                        return (
                                            <option key={postType._id}>{postType.typeName}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" value={postCategoryId} onChange={e => setPostCategoryId(e.target.value)}>
                                {
                                    postCategories.map(postCategory => {
                                        return (
                                            <option key={postCategory._id}>{postCategory.categoryName}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Amount</Form.Label>
                            <Form.Control readOnly type="number" placeholder="Enter the amount value of your post" name="amount" value={amount} onChange={e => setAmount(e.target.value)}  />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Post Notes (Optional)</Form.Label>
                            <Form.Control type="text" placeholder="ex: Gas Fee, Grocery, make it specific!" name="postName" value={postName} onChange={e => setPostName(e.target.value)}  />
                        </Form.Group>

                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Edit Post</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}