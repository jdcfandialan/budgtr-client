import {useState, useEffect} from 'react';
import {ListGroup, Form, Button, Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';

export default function CreatePost({accountName, balance, accountId, type, currency, refreshPost, refreshAccounts, setActiveAccount, activeAccount}){
    const [postName, setPostName] = useState('');
    const [amount, setAmount] = useState('');
    const [postTypeId, setPostTypeId] = useState('Income');
    const [postCategoryId, setPostCategoryId] = useState('Salary');
    const [postTypes, setPostTypes] = useState([])
    const [postCategories, setPostCategories] = useState([]);
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    //fetch all post Types
    useEffect(()=>{
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/postTypes`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setPostTypes(data)
            }

            else setPostTypes([])
        })
    }, [])

    //fetch all post categories
    useEffect(()=>{
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/postCategories`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setPostCategories(data)
            }

            else setPostCategories([])
        })
    }, [show])

    //form validation
    useEffect(() => {
        if(amount > 0){
            setIsActive(true);
        }

        else setIsActive(false);
    }, [amount])

    function createPost(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/newPost`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                postName: postName,
                amount: amount,
                postTypeId: postTypeId,
                postCategoryId: postCategoryId,
                accountId: accountId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                let tempAmount = amount;
                if(postTypeId === 'Expenditure'){
                    tempAmount *= -1;
                }
                const newBalance = parseFloat(balance) + parseFloat(tempAmount);

                fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/${accountId}`,{
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        bankName: accountName,
                        amount: newBalance,
                        accountTypeId: type,
                        accountCurrencyId: currency
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        refreshPost();
                        refreshAccounts();
                        handleClose();
                        setActiveAccount({
                            ...activeAccount,
                            amount: newBalance
                        })
                        Swal.fire('Success!', 'Post successfully created!', 'success')
                    }
                })
            }

            else{
                handleClose()
                Swal.fire('Error!', 'Failed to create post!', 'error')
            }
        })
    }

    return (
        <React.Fragment>
            <ListGroup.Item as="li" className="mt-3 mb-1" action onClick={handleShow} className="d-flex align-items-center justify-content-between">
                Add a Post
                <Button size="sm" variant="light" onClick={handleShow}><img src="/add.png" width="24rem" height="24rem" /></Button>
            </ListGroup.Item>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a Post</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createPost(e)}>
                        <Form.Group>
                            <Form.Label>Type (Income or Expenditure)</Form.Label>
                            <Form.Control as="select" value={postTypeId} onChange={e => setPostTypeId(e.target.value)}>
                                {
                                    postTypes.map(postType => {
                                        return (
                                            <option key={postType._id}>{postType.typeName}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Category</Form.Label>
                            <Form.Control as="select" value={postCategoryId} onChange={e => setPostCategoryId(e.target.value)}>
                                {
                                    postCategories.map(postCategory => {
                                        return (
                                            <option key={postCategory._id}>{postCategory.categoryName}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Amount</Form.Label>
                            <Form.Control type="number" placeholder="Enter the amount value of your post" name="amount" value={amount} onChange={e => setAmount(e.target.value)}  />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Post Notes (Optional)</Form.Label>
                            <Form.Control type="text" placeholder="ex: Gas Fee, Grocery, make it specific!" name="postName" value={postName} onChange={e => setPostName(e.target.value)}  />
                        </Form.Group>

                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Create Post</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}