import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteAccountButton({setShowDetails, setActiveAccount, account, refreshAccounts}){
    
    function deleteAccount(e){
        e.stopPropagation();
        
        Swal.fire({
            title: `Delete ${account.bankName}?`,
            text: 'This will permanently remove your account from the database. Continue?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Delete'
        })
        .then(result => {
            if(result.value){
                fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/${account._id}`, {
                    method: 'DELETE',
                    headers:{
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        setShowDetails(false);
                        setActiveAccount({});
                        refreshAccounts();
                        Swal.fire('Deleted!', 'Account has been successfully deleted', 'success')
                    }
                })
            }
        })
    }

    return (
        <Button size="sm" variant="light" onClick={deleteAccount}>
            <img src="/criss-cross.png" width="24rem" height="24rem" />
        </Button>
    )
}