import {useState, useEffect} from 'react';
import Router from 'next/router';
import {Modal, Form, Nav, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddAcountType(){
    const [typeName, setTypeName] = useState('');
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    useEffect(() => {
        if(typeName !== ''){
            setIsActive(true);
        }

        else setIsActive(false);
    }, [typeName])

    function createAccountType(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/createNewAccountType`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                typeName: typeName
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire('Success!', 'New Account Type added successfully!', 'success')
                handleClose()
            }

            else{
                Swal.fire('Error!', 'Failed to add a new Account Type!', 'error')
                handleClose()
            }
        })
    }

    return (
        <React.Fragment>
            <Nav.Link onClick={handleShow}>Add an Account Type</Nav.Link>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add an Account Type</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createAccountType(e)}>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Enter Type Name" name="typeName" value={typeName} onChange={e => setTypeName(e.target.value)} />
                        </Form.Group>
                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Add Account Type</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}