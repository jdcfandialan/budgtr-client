import {useState, useEffect} from 'react';
import Router from 'next/router';
import {Modal, Form, Nav, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddAcountCurrency(){
    const [currencyAbb, setCurrencyAbb] = useState('');
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    useEffect(() => {
        if(currencyAbb !== '' && currencyAbb.length === 3){
            setIsActive(true);
        }

        else setIsActive(false);
    }, [currencyAbb])

    function createAccountType(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/createNewAccountCurrency`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                currencyAbb: currencyAbb
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire('Success!', 'New Account Currency added successfully!', 'success')
                Router.push('/home')
                handleClose()
            }

            else{
                Swal.fire('Error!', 'Failed to add a new Account Currency!', 'error')
                handleClose()
            }
        })
    }

    return (
        <React.Fragment>
            <Nav.Link onClick={handleShow}>Add an Account Currency</Nav.Link>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add an Account Currency</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createAccountType(e)}>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Enter 3 Letter Currency Abbreviation" name="currencyAbb" value={currencyAbb} onChange={e => setCurrencyAbb(e.target.value)} />
                        </Form.Group>
                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Add Account Currency</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}