import {useState, useEffect} from 'react';
import {Doughnut} from 'react-chartjs-2';

export default function TrendChart(){
    const [userPosts, setUserPosts] = useState([]);
    let totalSalary = 0;
    let totalDeposits = 0;
    let totalBills = 0;
    let totalEntertainment = 0;
    let totalFood = 0;
    let totalHealthcare = 0;
    let totalTransportation = 0;
    let totalGeneral = 0;
    let totalMisc = 0;

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res =>  res.json())
        .then(data => {
            if(data){
                setUserPosts(data)
            }

            else setUserPosts([])
        })
    }, [])

    const salaryPosts = userPosts.filter(post => post.postCategoryId === 'Salary');
    const depositPosts = userPosts.filter(post => post.postCategoryId === 'Deposit');
    const billsPosts = userPosts.filter(post => post.postCategoryId === 'Bills');
    const entertainmentPosts = userPosts.filter(post => post.postCategoryId === 'Entertainment/Hobbies');
    const foodPosts = userPosts.filter(post => post.postCategoryId === 'Food');
    const healthcarePosts = userPosts.filter(post => post.postCategoryId === 'Healthcare');
    const transportationPosts = userPosts.filter(post => post.postCategoryId === 'Transportation');
    const generalPosts = userPosts.filter(post => post.postCategoryId === 'General');
    const miscPosts = userPosts.filter(post => post.postCategoryId === 'Miscellaneous');
    
    totalSalary = salaryPosts.reduce(
        (total, salaryPost) => total + salaryPost.amount, // accumulator function
        parseFloat(totalSalary) // initial value
    )
    totalDeposits = depositPosts.reduce(
        (total, depositPost) => total + depositPost.amount, // accumulator function
        parseFloat(totalDeposits) // initial value
    )
    totalBills = billsPosts.reduce(
        (total, billsPost) => total + billsPost.amount, // accumulator function
        parseFloat(totalBills) // initial value
    )
    totalEntertainment = entertainmentPosts.reduce(
        (total, entertainmentPost) => total + entertainmentPost.amount, // accumulator function
        parseFloat(totalEntertainment) // initial value
    )

    totalFood = foodPosts.reduce(
        (total, foodPost) => total + foodPost.amount, // accumulator function
        parseFloat(totalFood) // initial value
    )

    totalHealthcare = healthcarePosts.reduce(
        (total, healthcarePost) => total + healthcarePost.amount, // accumulator function
        parseFloat(totalHealthcare) // initial value
    )

    totalTransportation = transportationPosts.reduce(
        (total, transportationPost) => total + transportationPost.amount, // accumulator function
        parseFloat(totalTransportation) // initial value
    )

    totalGeneral = generalPosts.reduce(
        (total, generalPost) => total + generalPost.amount, // accumulator function
        parseFloat(totalGeneral) // initial value
    )

    totalMisc = miscPosts.reduce(
        (total, miscPost) => total + miscPost.amount, // accumulator function
        parseFloat(totalMisc) // initial value
    )
    
    return (
        <Doughnut 
            data = {
                {
                    datasets: [
                        {
                            data: [totalSalary, totalDeposits, totalBills, totalEntertainment, totalFood, totalHealthcare, totalTransportation, totalGeneral, totalMisc],
                            
                            backgroundColor: ['#ff073', '#b30098', '#005c8a', '#07a698', '#1fc273', '#048010', '#93e82c', '#ffa90a', '#ff0000']
                        }
                    ],

                    labels: ["Salary", "Deposits", "Bills", "Entertainment/Hobbies", "Food", "Healthcare", "Transportation", "General", "Miscellaneous"]
                }
            }

            redraw = {false}
        />
    )
}