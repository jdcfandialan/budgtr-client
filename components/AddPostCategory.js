import {useState, useEffect} from 'react';
import Router from 'next/router';
import {Modal, Form, Button, Nav} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddPostCategory(){
    const [categoryName, setCategoryName] = useState('');
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    useEffect(() => {
        if(categoryName !== ''){
            setIsActive(true);
        }

        else setIsActive(false);
    }, [categoryName])

    function createPostCategory(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/createNewPostCategory`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                categoryName: categoryName
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire('Success!', 'New Post Category added successfully!', 'success')
                Router.push('/home')
                handleClose()
            }

            else{
                Swal.fire('Error!', 'Failed to add a new Post Category!', 'error')
                handleClose()
            }
        })
    }

    return (
        <React.Fragment>
            <Nav.Link onClick={handleShow}>Add a Post Category</Nav.Link>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a Post Category</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createPostCategory(e)}>
                        <Form.Group>
                            <Form.Control type="text" placeholder="Enter Category Name" name="categoryName" value={categoryName} onChange={e => setCategoryName(e.target.value)} />
                        </Form.Group>
                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Add Post Category</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}