import {useState, useEffect} from 'react';
import {ListGroup, Modal, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function CreateAccount({refreshAccounts}){
    const [bankName, setBankName] = useState('');
    const [amount, setAmount] = useState('');
    const [accountTypeId, setAccountTypeId] = useState('Cash-on-Hand');
    const [accountCurrencyId, setAccountCurrencyId] = useState('PHP');
    const [accountTypes, setAccountTypes] = useState([]);
    const [accountCurrencies, setAccountCurrencies] = useState([]);
    const [show, setShow] = useState(false);
    const [isActive, setIsActive] = useState(false);

    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    //fetch all account types
    useEffect(()=>{
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/accountTypes`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setAccountTypes(data)
            }

            else setAccountTypes([])
        })
    }, [])

    useEffect(()=> {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/accountCurrencies`)
        .then(res => res.json())
        .then(data => {
            if(data){
                setAccountCurrencies(data)
            }
            
            else setAccountCurrencies([])
        })
    }, [])

    //form validation
    useEffect(() => {
        if(bankName !== '' && amount > 0){
            setIsActive(true)
        }

        else setIsActive(false)
    },[bankName, amount])

    function createAccount(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/newAccount`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                bankName: bankName,
                amount: amount,
                accountTypeId: accountTypeId,
                accountCurrencyId: accountCurrencyId
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                refreshAccounts()
                Swal.fire('Success!', 'Account successfully added!', 'success')
                handleClose()
            }

            else{
                Swal.fire('Error!', 'Failed to create account!', 'error')
                handleClose()
            }
        })
    }

    return (
        <React.Fragment>
            <ListGroup.Item as="li" className="mb-3" action onClick={handleShow} className="d-flex align-items-center justify-content-between">
                Add an Account
                <Button size="sm" variant="light" onClick={handleShow}><img src="/add.png" width="24rem" height="24rem" /></Button>
            </ListGroup.Item>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} animation={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add an Account</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form onSubmit={e => createAccount(e)}>
                        <Form.Group>
                            <Form.Label>Account Label</Form.Label>
                            <Form.Control type="text" placeholder="ex: My BPI, Cash on Hand" name="bankName" value={bankName} onChange={e => setBankName(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Account Balance</Form.Label>
                            <Form.Control type="number" placeholder="ex: 100000" name="amount" value={amount} onChange={e => setAmount(e.target.value)} />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Account Type (Savings, Checking, etc.)</Form.Label>
                            <Form.Control as="select" value={accountTypeId} onChange={e => setAccountTypeId(e.target.value)}>
                                {
                                    accountTypes.map(accountType => {
                                        return (
                                        <option key={accountType._id}>{accountType.typeName}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Currency</Form.Label>
                            <Form.Control as="select" value={accountCurrencyId} onChange={e => setAccountCurrencyId(e.target.value)}>
                                {
                                    accountCurrencies.map(accountCurrency => {
                                        return (
                                        <option key={accountCurrency._id}>{accountCurrency.currencyAbb}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>
                        {
                            (isActive) ?
                            <Button variant="outline-primary" type="submit">Create Account</Button> : <Button variant="outline-danger" onClick={handleClose}>Close</Button> 
                        }
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}