import {Row, Col} from 'react-bootstrap';
import Login from '../components/Login';

export default function Home() {
    //style={{backgroundImage: "url('/bg-index.jpg')", objectFit: 'cover', minHeight: 'calc(100vh - 57px)'}}
    
    return (
        <Row>
            <Col lg={8} className="d-flex flex-column align-items-start justify-content-center">
                <h1 className="pl-5" style={{fontSize: '7rem'}}>budgtr.</h1>
                <h6 className="pl-5 pt-2" style={{fontSize: '1.55rem'}}>your buddy in budgeting.</h6>
            </Col>

            <Col lg={4} className="d-flex flex-column justify-content-center">
                <Login />
            </Col>
        </Row>
    )
}