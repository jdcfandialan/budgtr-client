import {useState, useEffect} from 'react';
import {UserProvider} from '../UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap';
import NavBar from '../components/NavBar';
import Head from 'next/head';
import '../styles/globals.css';

export default function MyApp({Component, pageProps}){
    //states for user
    const [user, setUser] = useState({id: null});

    const unsetUser = () => {
        localStorage.clear();

        setUser({
            id: null
        })
    }

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
        }).then(res => res.json())
        .then(data => {
            if(data._id){
                setUser({
                    id: data._id
                })
            }

            else{
                id: null
            }
        })
    }, [user.id])
  
    return(
    <React.Fragment>
        <Head>
            <title>budgtr.</title>
            <link rel="icon" href="/budgtr-icon.png" />
        </Head>
        
        <UserProvider value={{user, setUser, unsetUser}}>
            <NavBar />

            <Container fluid>
                <Component {...pageProps} />
            </Container>
        </UserProvider>
    </React.Fragment>
    )
}
