import {useState, useEffect} from 'react';
import {Row, Col, ListGroup, Jumbotron} from 'react-bootstrap';
import CreateAccount from '../../components/CreateAccount';
import AccountDetails from '../../components/AccountDetails';
import CreatePost from '../../components/CreatePost';
import PostDetails from '../../components/PostDetails';
import UserTrends from '../../components/UserTrends';

export default function index(){
    const [showDetails, setShowDetails] = useState(false);
    const [activeAccount, setActiveAccount] = useState({});
    const [accountPosts, setAccountPosts] = useState([]);
    const [userAccounts, setUserAccounts] = useState([]);

    //fetch all the posts under the selected account
    const refreshPost = () => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/posts/${activeAccount._id}`)
        .then(res => res.json())
        .then(data => {
            if(data){
                let currentBalance = activeAccount.amount;

                const postsWithRunning = data.map(post => {
                    const prevBalance = currentBalance
                    let tempBalance = post.amount

                    if(post.postTypeId === 'Expenditure'){
                        tempBalance *= -1;
                    }

                    currentBalance = prevBalance - tempBalance

                    return {
                        ...post,
                        runningBalance: prevBalance
                    }
                })

                setAccountPosts(postsWithRunning)
            }

            else setAccountPosts([]);
        })
    }

    const refreshAccounts = () => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/accounts/`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                setUserAccounts(data)
            }

            else setUserAccounts([]);
        })
    }

    useEffect(() => {
        refreshAccounts()
    }, [activeAccount])

    useEffect(() => {
        refreshPost()
    }, [activeAccount])

    const filterPosts = accountPosts.filter(post => post.accountId === activeAccount._id);

    return (
        <Row>
            <Col lg={3}>
                <ListGroup className="mt-2">
                    <UserTrends />
                    <CreateAccount refreshAccounts={refreshAccounts} />
                    {
                       userAccounts.map(account => {
                           return (
                               <AccountDetails
                                    refreshAccounts={refreshAccounts}
                                    account={account}
                                    setShowDetails={setShowDetails}
                                    setActiveAccount={setActiveAccount}
                                    activeAccount={activeAccount}
                                    key={account._id}
                                />
                           )
                       })
                    }
                </ListGroup>
            </Col>

            <Col lg={9}>
                {
                    (showDetails) ? 
                    <React.Fragment>
                        <div className="d-flex align-items-center justify-content-between">
                            <h3>{activeAccount.bankName}</h3>
                            <h6>Balance: {activeAccount.accountCurrencyId} {activeAccount.amount}</h6>
                            <h6>Account Type: {activeAccount.accountTypeId}</h6>
                        </div>
                        
                        <ListGroup>
                            <CreatePost
                                accountName={activeAccount.bankName}
                                balance={activeAccount.amount}
                                accountId={activeAccount._id}
                                type={activeAccount.accountTypeId}
                                currency={activeAccount.accountCurrencyId}
                                refreshPost={refreshPost}
                                refreshAccounts={refreshAccounts}
                                setActiveAccount={setActiveAccount}
                                activeAccount={activeAccount}
                            />
                            {
                                filterPosts.map(accountPost => {
                                    return (
                                        <PostDetails
                                            key={accountPost._id}
                                            accountPost={accountPost}
                                            currencyId={activeAccount.accountCurrencyId}
                                            account={activeAccount}
                                            balance={activeAccount.amount}
                                            refreshPost={refreshPost}
                                            refreshAccounts={refreshAccounts}
                                            activeAccount={activeAccount}
                                            setActiveAccount={setActiveAccount}
                                            oldAmount={accountPost.amount}
                                            oldTypeId={accountPost.postTypeId}
                                        />
                                    )
                                })
                            }
                        </ListGroup>
                    </React.Fragment>
                    
                    :

                    <Jumbotron className="mt-2">
                        <h1>Get started!</h1>
                        <ul>
                            <li>Select Add an Account to create a new account</li>
                            <li>Select an account name to view details</li>
                            <li>Press Show User Trends to display a chart of your trends according to the category of your posts</li>
                            <li>Posts you create under each account automatically reflect on that account's balance</li>
                        </ul>
                    </Jumbotron>
                }
            </Col>
        </Row>
    )
}